<?php
namespace Rukka\Vibot;

use Rukka\Vibot\Converter\InvoiceAttachmentHtmlConverter;
use Rukka\Vibot\Converter\InvoiceAttachmentPdfConverter;


class InvoiceAttachment
{

    private $converter = [
        'html' => InvoiceAttachmentHtmlConverter::class,
        'pdf' => InvoiceAttachmentPdfConverter::class,
    ];

    private $pathToTemplate = __DIR__ . '/../src/views/invoice_attachments_template.php';
    private $outputPath = '/tmp/invoice';
    private $data = null;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function setTemplate($pathToTemplate)
    {
        return $this->pathToTemplate = $pathToTemplate;
    }

    public function setPath($outputPath)
    {
        return $this->outputPath = $outputPath;
    }

    public function generate($output = 'pdf')
    {
        $file = new $this->converter[$output]($this->data, $this->pathToTemplate, $this->outputPath . '.' . $output);

        return $file->generate();
    }

    public function output($output = 'pdf')
    {
        $file = new $this->converter[$output]($this->data, $this->pathToTemplate, $this->outputPath . '.' . $output);
        $file->output();
    }

}