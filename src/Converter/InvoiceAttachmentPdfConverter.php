<?php
namespace Rukka\Vibot\Converter;

use Dompdf\Dompdf;
use Rukka\Vibot\Converter\InvoiceAttachmentHtmlConverter;

class InvoiceAttachmentPdfConverter implements InvoiceAttachmentConverterInterface
{
    private $template;
    private $data;
    private $outputPath;
    private $htmlParser;

    public function __construct($data, $pathToTemplate, $outputPath)
    {
        $this->dompdf = new Dompdf;
        $this->htmlParser = new InvoiceAttachmentHtmlConverter($data, $pathToTemplate, $outputPath);
        $this->data = $data;
        $this->pathToTemplate = $pathToTemplate;
        $this->outputPath = $outputPath;
    }

    public function generate()
    {
        return null;
    }

    public function output()
    {
        $this->build($this->htmlParser->generate());

        file_put_contents($this->outputPath, $this->dompdf->output());
    }

    private function build($htmlFile)
    {
        $this->configPdf();
        $this->dompdf->loadHtml($htmlFile);
        $this->dompdf->render();
    }

    private function configPdf()
    {
        $this->dompdf->setPaper('A4', 'horizontal');
    }
}