<?php
namespace Rukka\Vibot\Converter;

interface InvoiceAttachmentConverterInterface
{
    public function generate(); //Ausgabe im Browser
    public function output(); //Ausgabe als Stream
}